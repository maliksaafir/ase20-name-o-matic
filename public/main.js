const LOADER_HTML = `<svg class="animate-spin ml-1 h-5 w-5 inline-block text-black" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
          <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
          <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
        </svg>`;

/**
 * Shuffles array in place.
 *
 * Lovingly taken from https://stackoverflow.com/a/6274381/1708147
 *
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
  return a;
}

/**
 * Checks whether two arrays are equal (have the same elements).
 * 
 * Taken from https://www.geeksforgeeks.org/check-if-two-arrays-are-equal-or-not/ (translated to JS).
 * @param {Array} a 
 * @param {Array} b 
 */
function areEqual(a, b) {
    if (a.length != b.length) {
        return false;
    }

    counts = {};
    for (let i in a) {
        let x = a[i];
        if (x in counts) {
            counts[x] += 1;
        } else {
            counts[x] = 1;
        }
    }

    for (let i in b) {
        let x = b[i];
        if (!(x in counts) || counts[x] == 0) {
            return false;
        } else {
            counts[x] -= 1;
        }
    }

    return true;
}

const pickRandom = (names) => {
  return names[Math.floor(Math.random() * names.length)];
};

const getNamesFromTextarea = (textarea) => {
  return textarea.value
    .split("\n")
    .map((x) => x.trim())
    .filter(Boolean);
};

const showLoader = (el) => {
  el.innerHTML = LOADER_HTML;
};

const showName = (el, name) => {
  el.textContent = name;
};

const main = () => {
  const elButton = document.getElementById("js-button");
  const elName = document.getElementById("js-name");
  const elTextarea = document.getElementById("js-textarea");

  let isLoading = false;

  let idx = 0;
  let shuffled = false;
  let shuffled_names = [];

  const onButtonClick = () => {
    if (isLoading) {
      return;
    }

    isLoading = true;
    showLoader(elName);

    setTimeout(() => {
      const names = getNamesFromTextarea(elTextarea);
      
      // check for changes to the names list
      if (!areEqual(names, shuffled_names)) {
          shuffled = false;
          idx = 0;
      }

      // shuffle the list
      if (!shuffled) {
        shuffled_names = shuffle(names);
        shuffled = true;
      }

      // show the next name in the shuffled list
      showName(elName, shuffled_names[idx]);
      idx = (idx + 1) % shuffled_names.length;

      isLoading = false;
    }, 400);
  };

  elButton.addEventListener("click", onButtonClick);
};

main();
